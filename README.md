# BWAT

Ce projet se présente sous la forme d'un site web, sur lequel un utilisateur peut se connecter ou s'inscrire via un formulaire sur une page.
Lors de l'inscription, l'utilisateur est enregistré dans une base de données en local, avec laquelle le site communique via une API.
Une fois authentifié, l'utilisateur est redirigé sur une page qui lui souhaite la bienvenue avec son nom et son prénom.
Des tests ont été mis en place pour contrôler la qualité du site.

## Docker

Un docker-compose a été mis en place pour faciliter le déploiement. Il crée trois containers :
- un container avec un serveur SQL
- un container avec une API
- un container avec une interface utilisateur

Pour configurer le déploiement, des variables d'environement sont nécessaires :
```env
DB_USERNAME= le nom d'utilisateur de MySQL à définir
DB_PASSWORD= le mot de passe de MySQL à définir
DB_DATABASE= le nom de la base de données pour le projet
STACK_NAME= nom du projet (bwat)
API_URL= URL public de l'API
```

## Front-end

Créez une application React avec la commande ```npx create-react-app front``` puis accédez à ce répertoire avec la commande ```cd front```

npx est l'exécuteur de package utilisé par npm pour exécuter des packages à la place d'une installation globale

Pages créées : sign up, login, Home(redirection)

Utilisez la methode "Axios" (un "API / promise-based HTTP client" qui permet de récuperer les données) via un appel d'API pour rendre les pages fonctionnelles

Ajoutez les dépendances grâce à la commande ```npm install``` ou la commande ```yarn install``` (si yarn installé globalement)

### Lancer l'application

Pour exécuter l'application en mode développement, exécutez la commande ```npm start``` ou la commande ```yarn start``` 

Rendez-vous sur ```[http://localhost:3000](http://localhost:3000)``` pour accéder à la page de connexion dans votre navigateur


### Tests avec Cypress 

Cypress est un framework JavaScript de “end to end testing(fonctionnel)”
Il permet de tester le rendu et les actions utilisateurs de l'application à l'aide de scénarios

Installez Cypress avec la commande ```npm install --save-dev cypress``` puis lancez-le avec la commande ```npx cypress open```

Pour configurer les tests e2e, naviguez à l'aide de chrome ou d'électron et commencez à tester
Pour ce faire, créez un fichier de test avec l'extension "Test.cy.js" ou dans le navigateur cypress en cliquant sur "+ New spec" 
Votre navigateur sera ensuite controlé par un logiciel de test automatisé

### Technologies utilisées

 - Langages : Javascript, HTML5, CSS3
 - Framework Front: React
 - Framework Test: Cypress

## API

L'API fonctionne avec MariaDB

### Mise en place

Installer un server SQL (MariaDB, MySQL, ...)

Pour faire fonctionner l'API, installez [go 1.19.5](https://go.dev/dl/) sur votre machine

Clonez le git ```git clone https://gitlab.com/tinhinaneachir/bwat.git```

Dupliquez le fichier exemple.env et rennomez-le en .env avant de remplir les champs
```.env
DATABASE_USERNAME= Nom d'utilisateur du serveur SQL
DATABASE_PASSWORD= Mot de passe du serveur SQl (s'il n'y en a pas, laissez vide)
DATABASE_ADDRESS= Adresse du serveur SQL
DATABASE_NAME= Nom de la base donnée à utiliser

PORT= Port de l'application GO

JWT_SECRET= Mot Secret qui chiffrera le token d'authentification
```

Démarrez Go avec la commande ```go run server.go```

### Technologies utilisées

 - Langage : Go
 - Framework API : gin
 - Frameork ORM : gorm
 - Tests : Go


### Documentation (Swagger)

Pour accéder à la doc de l'API, rendez-vous sur ```localhost:"PORT"/api/v1/swagger/index.html```

## CI/CD

### Sonarqube

SonarQube est un logiciel de révision de code
Le but d'utiliser sonarqube est:
- identifier des duplications de code
- afficher l'état de l'application
- fournir un code propre (fiable, sécurisé, maintenable, lisible et modulaire)
- mesurer le niveau de documentation et connaître la couverture de test déployée

Liens de référence :
- Site internet: https://www.sonarsource.com/products/sonarqube/
- Téléchargement: https://www.sonarsource.com/products/sonarqube/downloads/
  NB: choisir la méthode  "à partir de l'image docker"
  ```$ docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest```
- Documentation: https://docs.sonarqube.org/latest/


