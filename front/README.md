## Getting Started with Create React App

npx create-react-app front

puis accédez à ce répertoire : cd front

npx est l'exécuteur de package utilisé par npm pour exécuter des packages à la place d'une installation globale.

Pages créé: Pages sign up, login, Home(redirection)
Utiliser la methode "Axios" - un "API / promise-based HTTP client" permettre de récuperer les données
via un call api pour rendre fonctionel les pages.


# To add dependencies

npm install ou yarn install (si yarn installé globalement)

## To run

Pour exécuter l'application en mode développement "npm start" ou "yarn start"

Ouvrir [http://localhost:3000](http://localhost:3000) pour le voir dans votre navigateur.


### Framework Cypress

Cypress est un framework JavaScript de “end to end testing(fonctionnel)”,
il permet de tester le rendu et les actions utilisateurs de l'application à l'aide de scénarios

## CYPRESS install

npm install --save-dev cypress

## Open Cypress

npx cypress open

## configure  e2e Testing

Naviguez à l'aide de chrome ou d'électron et commencez à tester ou changez de type de test
puis chrome ou le navigateur choisi est controlé par un logiciel de test automatisé

## To create test file

créer le fichier test avec l'extension "Test.cy.js" ou dans le navigateur cypress en cliquant "+ New spec"


### Technologies utilisées

- Langage : Javascript
- Html 5 et css 3
- Framework Front: React
- Framework Test:Cypress

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
