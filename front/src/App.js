import './App.css';
import Login from "./Login";
import {Route, Routes} from "react-router-dom";
import Register from "./Register";
import Home from "./Home";


function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/" element={ <Login/> } />
                <Route path="register" element={ <Register/> } />
                <Route path="home" element={<Home/>}/>
            </Routes>
        </div>

    );
}

export default App;
