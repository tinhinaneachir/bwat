import './App.css';
import profile from "./image/profile.png";
import email from "./image/email.jpeg";
import pwd from "./image/pwd.png";
import nameImage from "./image/nameImg.png";
import {Link} from "react-router-dom";
import axios from "axios";
import React, {useState} from "react";

function Register() {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [userEmail, setUserEmail] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const submitForm = (e) => {
        const validForm = [firstname, lastname, userEmail, userPassword].every((e) => e.length > 0);
        if (validForm) {
            axios
                .post(process.env.REACT_APP_BASE_URL_API + "/signup", {
                    firstname: firstname,
                    lastname: lastname,
                    email: userEmail,
                    password: userPassword,
                })
                .then((res) => {
                    alert("user ajouté");

                })
                .then(() => {
                    setFirstname("");
                    setLastname("");
                    setUserEmail("");
                    setUserPassword("");
                })
                .catch(() => alert("error"));
        }
        e.preventDefault();
    };
    return (
        <div className="main">
            <div className="sub-main">
                <div>
                    <div className="imgs">
                        <div className="container-image">
                            <img src={profile} alt="profile" className="profile"/>
                        </div>

                    </div>
                    <div>
                        <h2>Register</h2>
                        <form onSubmit={submitForm}>
                            <div>
                                <img src={nameImage} alt="firstname" className="icon"/>
                                <input type="text" placeholder="firstname " className="inputName"
                                       value={firstname}
                                       onChange={(event) => {
                                           setFirstname(event.target.value);
                                       }}/>
                            </div>
                            <div className="secInput">
                                <img src={nameImage} alt="lastname" className="icon"/>
                                <input type="text" placeholder="lastname " className="inputName"
                                       value={lastname}
                                       onChange={(event) => {
                                           setLastname(event.target.value);
                                       }}/>
                            </div>
                            <div className="secInput">
                                <img src={email} alt="email" className="icon"/>
                                <input type="email" placeholder="email " className="inputName"
                                       value={userEmail}
                                       onChange={(event) => {
                                           setUserEmail(event.target.value);
                                       }}/>
                            </div>
                            <div className="secInput">
                                <img src={pwd} alt="password" className="icon"/>
                                <input type="password" placeholder="Password" className="inputName"
                                       value={userPassword}
                                       onChange={(event) => {
                                           setUserPassword(event.target.value);
                                       }}/>
                            </div>
                            <div className="login-button">
                                <button>Sign Up</button>
                            </div>
                        </form>

                        <p className="linkText">Already have an account?
                            <Link to="/">Login</Link>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;