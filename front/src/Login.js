import './App.css';
import profile from "./image/profile.png";
import pwd from "./image/pwd.png"
import React, {useState } from "react";
import email from "./image/email.jpeg"
import {Link, useNavigate} from "react-router-dom";
import axios from "axios";



function Login() {
    const [userEmail, setUserEmail] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const navigate = useNavigate();

    const submitForm = (e) => {
        const validForm = [userEmail, userPassword].every((e) => e.length > 0);
        if (validForm) {
            axios
                .post(process.env.REACT_APP_BASE_URL_API + "/signin", {
                    email: userEmail,
                    password: userPassword,
                })
                .then((res)=>{
                    console.log("reussi");
                    localStorage.setItem("bwat-token", res.data.token);

                })
                .then(() => navigate("/home"))
                .catch(() => alert("Vos identifiants sont incorrects"));
        } else alert("Un des champs est incomplet");
        e.preventDefault();
    };
    return (
        <div className="main">
            <div className="sub-main">
                <div>
                    <div className="imgs">
                        <div className="container-image">
                            <img src={profile} alt="profile" className="profile"/>
                        </div>

                    </div>
                    <div>
                        <h2>Login</h2>
                        <form onSubmit={submitForm}>
                        <div>
                            <img src={email} alt="email" className="icon"/>
                            <input type="email" placeholder="email " className="inputName"
                                   value={userEmail}
                                   onChange={(event) => {
                                       setUserEmail(event.target.value);
                                   }}/>
                        </div>
                        <div className="secInput">
                            <img src={pwd} alt="password" className="icon"/>
                            <input type="password" placeholder="password" className="inputName"
                                   value={userPassword}
                                   onChange={(event) => {
                                       setUserPassword(event.target.value);
                                   }}/>
                        </div>
                        <div className="login-button">
                            <button>Sign In</button>
                        </div>
                        </form>

                        <p className="linkText"> Don't have an account?
                            <Link to="register">Sign Up</Link>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
