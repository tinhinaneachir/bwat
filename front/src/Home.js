import './App.css';
import React, {useEffect, useState} from "react";
import axios from "axios";

function Home() {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    useEffect(() => {
        axios
            .get(process.env.REACT_APP_BASE_URL_API + "/me",{
                headers : { Authorization: `Bearer ${localStorage.getItem("bwat-token")}`}
            })
                .then((res) => {
                    setFirstname(res.data.firstname);
                    setLastname(res.data.lastname);
                })
                .catch(() => alert("not connected"))
    })
    return (
        <div className="home">
            <h1>You are redirected to this page successfully welcome <span className="userName">{firstname} {lastname}</span>  </h1>

        </div>
    );


}

export default Home;