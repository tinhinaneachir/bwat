describe("visit localhost", () => {
    beforeEach(() => {
        cy.viewport(1280, 720)
        cy.visit('http://localhost:3000/');
    })
    it("Test attribute href exist", () => {
        cy.get('a').invoke('attr', 'href').should('eq', '/register');
    })
    it("Test input contains classname", () => {
        cy.get('input').first().should("have.class", "inputName")

    })
    it("Test input does not contain element ID", () => {
        cy.get('input').should('have.length', 2)
        cy.get('input').first().should("not.have.id")
    })

    it("Type email  and password ", () => {
        cy.get('input').first().type("g@gmail.com");
        cy.get('input').last().type("hello")

    })
    it("click button Sign In  ", () => {
        cy.get('button').should("exist");
        cy.get('button').should("not.have.class");
        cy.get('button').click();
    })
    it("Test img attribute",()=>{
      cy.get('img').should('have.attr', 'alt','profile');
        cy.get('img').eq(1).should('have.attr', 'alt','email');
        cy.get('img').eq(2).should('have.attr', 'alt','password');

    })
    it("Test H2 element",()=>{
        cy.get('h2').contains('Login');
    })

})