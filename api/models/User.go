package models

import "gorm.io/gorm"

/*
/!\ Warning /!\
Fields must be in CamelCase (and SnakeCase in the database)
Besides, the model name in the database must contain a 's' at the end (otherwise, problems will come)
*/

// User
//
//	 @Definition User
//		@Property Lastname string false "User's last name"
//		@Property Firstname string false "User's first name"
//		@Property Email string true "User's e-mail"
//		@Property Password string true "User's password"
type User struct {
	gorm.Model
	Lastname  string
	Firstname string
	Email     string `gorm: unique`
	Password  string
}
