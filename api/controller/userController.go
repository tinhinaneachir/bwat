package controller

import (
	"api/initializer"
	"api/models"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"os"
	"time"
)

// Signup		godoc
// @Summary		Sign up a new user
// @Description	A request to add a user
// @Param 		user body models.User true "User's infos"
//
//	@ParamExample {json} Query example
//	  {
//	    "email": "timote.lefou@example.com",
//	    "password": "secret123",
//	    "first_name": "Timoté",
//	    "last_name": "Lefou"
//	  }
//
// @Tags		Account
// @Accept		json
// @Produce		json
// @Success 	201 {string}  string "Signed up"
// @Failure		400 {string}  string "Failed to read body or hash password"
// @Failure		500 {string}  string "Failed to create user"
// @Router		/api/v1/signup [post]
func Signup(g *gin.Context) {
	var body struct {
		Firstname string
		Lastname  string
		Email     string
		Password  string
	}

	if g.Bind(&body) != nil {
		g.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)

	if err != nil {
		g.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed hash password",
		})

		return
	}

	user := models.User{
		Firstname: body.Firstname,
		Lastname:  body.Lastname,
		Email:     body.Email,
		Password:  string(hash),
	}

	result := initializer.DB.Create(&user)

	if result.Error != nil {
		g.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to create user",
		})

		return
	}

	g.JSON(http.StatusCreated, gin.H{})
}

// Signin		godoc
// @Summary		Log in with token
// @Description A request to log in a user
// @Param 		user body models.User true "User's infos"
//
//	@ParamExample {json} Query example
//	  {
//	    "email": "timote.lefou@example.com",
//	    "password": "secret123",
//	  }
//
// @Tags		Account
// @Accept		json
// @Produce		json
// @Success		200 {string}  string "Signed in"
// @SuccessExample {json} Success example
//
//	HTTP/1.1 200 OK
//	{
//	  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
//	}
//
// @Failure		400 {string}  string "Authentification failed"
// @Failure		401 {string}  string "Invalid password or email"
// @Failure		500 {string}  string "Failed to create token"
// @Router		/api/v1/signin [post]
func Signin(g *gin.Context) {
	var body struct {
		Email    string
		Password string
	}

	if g.Bind(&body) != nil {
		g.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}

	var user models.User
	initializer.DB.First(&user, "email = ?", body.Email)

	if user.ID == 0 {
		g.JSON(http.StatusUnauthorized, gin.H{
			"error": "Invalid password or email",
		})
		return
	}
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(body.Password))

	if err != nil {
		g.JSON(http.StatusUnauthorized, gin.H{
			"error": "Invalid password or email",
		})

		return
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.ID,
		"exp": time.Now().Add(time.Hour * 24 * 30).Unix(),
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))

	if err != nil {
		g.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to create token",
		})
		log.Print(err)

		return
	}

	g.SetSameSite(http.SameSiteLaxMode)
	g.JSON(http.StatusOK, gin.H{
		"token": tokenString,
	})

}

// Me			godoc
// @Summary		Show connected user
// @Description A request to show the connected user
// @Tags		Account
// @Accept		json
// @Produce		json
// @Success		200 {array} string "Show user infos"
// @Router		/api/v1/me [get]
func Me(g *gin.Context) {
	user, _ := g.Get("user")
	g.JSON(http.StatusOK, gin.H{
		"firstname": user.(models.User).Firstname,
		"lastname":  user.(models.User).Lastname,
	})
}
