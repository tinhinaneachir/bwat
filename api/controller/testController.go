package controller

import "github.com/gin-gonic/gin"

// Helloworld 	godoc
// @Summary		Ping example
// @Description	A request to make a simple ping
// @Tags		Test
// @Accept		json
// @Produce		json
// @Success		200 {string} string "Hello world !"
// @Router		/api/v1/helloworld [get]
func Helloworld(g *gin.Context) {
	g.JSON(200, gin.H{
		"message": "Hello world !",
	})
}
