package test

import (
	"api/controller"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	_ "github.com/stretchr/testify/mock"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Set up router to allowed http requests
func SetUpRouter() *gin.Engine {
	router := gin.Default()
	return router
}

// Test the request
func TestPageHandler(t *testing.T) {
	mockResponse := `{"message":"hello world"}`
	r := SetUpRouter()
	r.GET("/test", controller.Helloworld)
	req, _ := http.NewRequest("GET", "/test", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	assert.Equal(t, mockResponse, string(responseData))
	assert.Equal(t, http.StatusOK, w.Code)
}
