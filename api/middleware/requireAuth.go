package middleware

import (
	"api/initializer"
	"api/models"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"time"
)

// Check if the access token is ok -> if it is, authorize the connection, otherwise, return an error
func RequireAuth(g *gin.Context) {
	// WARNING /!\ space at the end in BREARER_SCHEMA is important !!!
	const BEARER_SCHEMA = "Bearer "
	authHeader := g.GetHeader("Authorization")
	tokenString := authHeader[len(BEARER_SCHEMA):]

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(os.Getenv("JWT_SECRET")), nil
	})

	if err != nil {
		g.AbortWithStatus(http.StatusUnauthorized)
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			g.AbortWithStatus(http.StatusUnauthorized)
		}

		var user models.User
		initializer.DB.First(&user, claims["sub"])

		if user.ID == 0 {
			g.AbortWithStatus(http.StatusUnauthorized)
		}

		g.Set("user", user)

		g.Next()
	} else {
		g.AbortWithStatus(http.StatusUnauthorized)
	}
}
