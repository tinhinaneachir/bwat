package main

import (
	"api/controller"
	_ "api/docs"
	"api/initializer"
	"api/middleware"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"       // swagger embed files
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
	"os"
)

// Call the three necessary functions to initialize environment, the database connection and synchronization
func init() {
	initializer.LoadEnv()
	initializer.Connect()
	initializer.SyncDatabase()
}

// @title			BWAT API
// @version     	1.0
// @description		This is a wonderful server built with love
// @license.name	Apache 2.0
// @license.url   	http://www.apache.org/licenses/LICENSE-2.0.html
// @host      		localhost
// @BasePath  		/api/v1
// @Schemes			http https
// @securityDefinitions.basic  BasicAuth
func main() {
	server := gin.Default()
	server.Use(middleware.Cors())

	v1 := server.Group("/api/v1")
	v1.GET("/helloworld", controller.Helloworld)
	v1.POST("/signup", controller.Signup)
	v1.POST("/signin", controller.Signin)
	v1.GET("/me", middleware.RequireAuth, controller.Me)
	v1.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	server.Run(":" + os.Getenv("PORT"))
}
