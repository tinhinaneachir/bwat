package initializer

import (
	"api/models"
	"log"
)

// Synchronize the database with the model or return an error
func SyncDatabase() {
	err := DB.AutoMigrate(&models.User{})
	if err != nil {
		log.Print("WARN : No migration")
	}
}
