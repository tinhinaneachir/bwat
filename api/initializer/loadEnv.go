package initializer

import (
	"github.com/joho/godotenv"
)

// LoadEnv Load the environment file or return an error
func LoadEnv() {
	err := godotenv.Load(".env")

	if err == nil {
		return
	}

}
