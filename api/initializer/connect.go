package initializer

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
)

var DB *gorm.DB

// Establish a connection with the database using the environment variables or return an error
func Connect() {
	var err error
	linked := os.Getenv("DATABASE_USERNAME") + ":" + os.Getenv("DATABASE_PASSWORD") + "@tcp(" + os.Getenv("DATABASE_ADDRESS") + ")/" + os.Getenv("DATABASE_NAME") + "?charset=utf8mb4&parseTime=true"
	DB, err = gorm.Open(mysql.Open(linked), &gorm.Config{})

	if err != nil {
		log.Fatal("no connection with database")
	}
}
